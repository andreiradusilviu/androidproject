package database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Email.class},version = 1)
public abstract class EmailDB extends RoomDatabase
{
    public abstract DAO dao();
}
