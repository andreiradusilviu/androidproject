package database;

import android.widget.ImageView;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Email")
public class Email
{
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String receiver;

    public void setDate(String date) {
        this.date = date;
    }

    private int imageId;

    private String sender;

    private String title;

    private String context;

    private String date;

    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Email.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Email other = (Email) obj;

        if(this.context.equals(other.context) && this.receiver.equals(other.receiver) && this.sender.equals(other.receiver) && this.title.equals(other.title))
            return true;

        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String name) {
        this.receiver = name;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
