package database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface DAO
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addEmail(Email email);

    @Query("SELECT * FROM Email")
    List<Email> getEmails();

    @Query("DELETE FROM Email")
    public void clear();
}
