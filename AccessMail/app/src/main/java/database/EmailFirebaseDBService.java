package database;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class EmailFirebaseDBService
{
    private FirebaseDatabase database;
    private DatabaseReference referenceEmails;
    private ArrayList<Email> emails = new ArrayList<Email>();

     public EmailFirebaseDBService()
     {
         database = FirebaseDatabase.getInstance();
         referenceEmails = database.getReference("Email");
     }

     public interface DataStatus
     {
         void DataIsLoaded(ArrayList<Email> emails,ArrayList<String> keys) throws ExecutionException, InterruptedException;
         void DataIsInserted();
         void DataIsUpdated();
         void DataIsDeleted();
     }

     public void readEmails(final DataStatus dataStatus)
     {
         referenceEmails.addValueEventListener(new ValueEventListener() {
             @Override
             public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 emails.clear();

                 ArrayList<String> keys = new ArrayList<String>();

                 for(DataSnapshot keyNode : dataSnapshot.getChildren())
                 {
                     keys.add(keyNode.getKey());

                     Email email = keyNode.getValue(Email.class);

                     if(!emails.contains(email))
                     emails.add(email);
                 }

                 try {
                     dataStatus.DataIsLoaded(emails,keys);
                 } catch (ExecutionException e) {
                     e.printStackTrace();
                 } catch (InterruptedException e) {
                     e.printStackTrace();
                 }
             }



             @Override
             public void onCancelled(@NonNull DatabaseError databaseError) {

             }
         });
     }

     public void addEmail(Email email,final DataStatus dataStatus)
     {
        String key = referenceEmails.push().getKey();
        referenceEmails.child(key).setValue(email).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.DataIsInserted();
            }
        });
     }
}
