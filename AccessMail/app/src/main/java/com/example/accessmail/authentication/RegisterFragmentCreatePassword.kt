package com.example.accessmail.authentication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.FragmentManager

import com.example.accessmail.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterFragmentCreatePassword : Fragment() {

    var passwordEditText: EditText? = null
    var repeatPasswordEditText: EditText? = null
    var finnishButton: Button? = null
    private var mFirebaseAuth : FirebaseAuth? = null
    private lateinit var database: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_register_create_password, container, false)

        passwordEditText = view.findViewById(R.id.createPasswordRegister)
        repeatPasswordEditText = view.findViewById(R.id.createConfirmPasswordRegister)
        finnishButton = view.findViewById(R.id.btnFinishConfirmedPassword)

        mFirebaseAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference


        finnishButton?.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                createAccount()
            }
        })
        return view
    }

    private fun createAccount()
    {
        var passwordString: String = ""
        var repeatPasswordString: String = ""

        if(passwordEditText != null && repeatPasswordEditText!= null)
        {
            passwordString = passwordEditText!!.text.toString()
            repeatPasswordString = repeatPasswordEditText!!.text.toString()
        }

        if(passwordString.isEmpty())
        {
            passwordEditText!!.error = "Please type an password"
            passwordEditText!!.requestFocus()
        }
        else if(passwordString.length < 6)
        {
            passwordEditText!!.error = "The password must contain at least 6 characters"
            passwordEditText!!.requestFocus()
        }
        else if(repeatPasswordString.isEmpty())
        {
            repeatPasswordEditText!!.error = "Please retype the password"
            repeatPasswordEditText!!.requestFocus()
        }
        else if(repeatPasswordString != passwordString)
        {
            repeatPasswordEditText!!.error = "The password does not match"
            repeatPasswordEditText!!.requestFocus()
        }
        else {
            val firstName = arguments?.getString("firstName", "")
            val lastName = arguments?.getString("lastName", "")
            val date = arguments?.getString("date", "")
            val gender = arguments?.getString("gender", "")
            val email = arguments?.getString("email", "")



            activity?.let {
                if (email != null) {
                    mFirebaseAuth?.createUserWithEmailAndPassword(email, passwordString)
                        ?.addOnCompleteListener(it) { task: Task<AuthResult> ->
                            if (task.isSuccessful) {

                                val uid = mFirebaseAuth?.currentUser?.uid
                                val user: User = User(uid, email, firstName, lastName, date, gender)
                                if (uid != null) {
                                    database.child("User").child(uid).setValue(user)

                                    Toast.makeText(
                                        activity, "Authentication succeeded.",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    activity?.supportFragmentManager?.popBackStack("REGISTER_FRAGMENT", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                    activity?.supportFragmentManager?.popBackStack()
                                }

                            } else {

                                Toast.makeText(
                                    activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            }

        }


    }


}
