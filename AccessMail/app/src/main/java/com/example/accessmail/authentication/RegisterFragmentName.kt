package com.example.accessmail.authentication

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.FragmentManager

import com.example.accessmail.R

class RegisterFragmentName : Fragment() {

    var buttonToRegisterDateOfBirth: Button?= null
    var firstNameEditText: EditText? = null
    var lastNameEditText: EditText? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_name, container, false)
        buttonToRegisterDateOfBirth = view.findViewById(R.id.btnToRegisterDateOfBirth)

        firstNameEditText = view.findViewById(R.id.firstName)
        lastNameEditText = view.findViewById(R.id.lastName)

        goToRegisterFragmentDateAndGender(view)

        return view
    }

    private fun goToRegisterFragmentDateAndGender(view: View){
        buttonToRegisterDateOfBirth!!.setOnClickListener(View.OnClickListener {

            if(isFirstLastNameEntered())
            {
                val firstNameString = firstNameEditText!!.text.toString()
                val lastNameString =  lastNameEditText!!.text.toString()

                val bundle = Bundle()
                bundle.putString("firstName", firstNameString)
                bundle.putString("lastName", lastNameString)

                val registerFragmentDateAndGender = RegisterFragmentDateAndGender()
                registerFragmentDateAndGender.arguments = bundle

                activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, registerFragmentDateAndGender).
                addToBackStack("REGISTER_FRAGMENT_DATE_AND_GENDER").commit()
            }
        })
    }

    private fun isFirstLastNameEntered(): Boolean {
        val firstNameString = firstNameEditText!!.text.toString()
        val lastNameString =  lastNameEditText!!.text.toString()

        if(firstNameString.isEmpty())
        {
            firstNameEditText!!.error = "Please enter your first name"
            firstNameEditText!!.requestFocus()
        }
        else if(lastNameString.isEmpty())
        {
            lastNameEditText!!.error = "Please enter your last name"
            lastNameEditText!!.requestFocus()
        }
        else
            return true

        return false;
    }


}
