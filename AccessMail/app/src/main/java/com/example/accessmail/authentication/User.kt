package com.example.accessmail.authentication

data class User (
    var uid: String? = "",
    var email: String? = "",
    val firstName: String? = "",
    val lastName: String? = "",
    val date: String? = "",
    val gender: String? = ""

)