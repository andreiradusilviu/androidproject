package com.example.accessmail.authentication

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.example.accessmail.R
import java.lang.StringBuilder
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragmentDateAndGender : Fragment() {

    var buttonToCreateMailAddress: Button? = null
    var dateEditText: EditText? = null
    var genderEditText: EditText? = null
    var isDateSelected = false
    var isGenderSelected = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_date_and_gender, container, false)
        buttonToCreateMailAddress = view.findViewById(R.id.btnToRegisterCreateMailAddress) as Button
        dateEditText = view.findViewById(R.id.editText_dateOfBirth)
        genderEditText = view.findViewById(R.id.editText_gender)

        isDateSelected = false
        isGenderSelected = false

        dateListener(view)
        genderListener(view)

        goToRegisterFragmentCreateMailAddress(view)
        return view;
    }

    private fun goToRegisterFragmentCreateMailAddress(view: View){
        buttonToCreateMailAddress!!.setOnClickListener(View.OnClickListener {

            if(!isDateSelected)
            {
                dateEditText!!.error = "PLease select a date"
                dateEditText!!.requestFocus()
            }
            else if(!isGenderSelected)
            {
                dateEditText!!.error = null
                genderEditText!!.error = "Please select a gender"
                genderEditText!!.requestFocus()
            }
            else if(isDateSelected && isGenderSelected)
            {
                genderEditText!!.error = null

                val dateString = dateEditText?.text.toString()
                val genderString = genderEditText!!.hint.toString()

                val bundle = arguments
                bundle?.putString("gender", genderString)
                bundle?.putString("date", dateString)

                val registerFragmentCreateMailAddress = RegisterFragmentCreateMailAddress()
                registerFragmentCreateMailAddress.arguments = bundle

                activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, registerFragmentCreateMailAddress).
                addToBackStack("REGISTER_FRAGMENT_CREATE_MAIL_ADDRESS").commit()
            }
        })
    }

    private fun dateListener(view: View)
    {

        dateEditText?.setOnClickListener(View.OnClickListener {

            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)


            activity?.let { it1 ->
                DatePickerDialog( it1,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        val dateFormat: StringBuilder = StringBuilder()
                        dateFormat.append(day.toString())
                            .append('/')
                            .append((month + 1).toString())
                            .append('/')
                            .append(year.toString())

                        dateEditText!!.setText(dateFormat.toString())
                        isDateSelected = true


                    }, year, month, day)
            }?.show()
        })
    }

    private fun genderListener(view: View)
    {

        genderEditText?.setOnClickListener(View.OnClickListener {
            val listItems = arrayOf("Male", "Female")
            val mBuilder = AlertDialog.Builder(activity)
            mBuilder.setTitle("Choose the gender")
            mBuilder.setSingleChoiceItems(listItems, -1) { dialogInterface, index ->
                genderEditText!!.hint = listItems[index].toString()
                isGenderSelected = true



                dialogInterface.dismiss()
            }
            mBuilder.setNeutralButton("Cancel") { dialog, which ->
                dialog.cancel()
            }

            val mDialog = mBuilder.create()
            mDialog.show()
        })

    }

}
