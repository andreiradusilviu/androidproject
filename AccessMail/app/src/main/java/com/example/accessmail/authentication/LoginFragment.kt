package com.example.accessmail.authentication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.accessmail.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import home_activity.HomeActivity
import welcome.WelcomeLogoActivity

class LoginFragment : Fragment() {

    private var register :TextView? = null
    private var loginButton: Button? = null
    private var usernameEditText: EditText? = null
    private var passwordEditText: EditText? = null
    private var keepLogged: CheckBox? = null
    private var mFirebaseAuth : FirebaseAuth? = null
    private val SHARED_PREFERENCES_NAME = "SHARED_PREFERENCE_INFO"
    private val EMAIL_FIELD = "EMAIL"
    private val PASSWORD_FIELD = "PASSWORD"
    private val KEEP_LOGGED_FIELD ="KEEP_LOGGED"

    private var mSharedPreference : SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        if (container != null) {
            mSharedPreference =
                context?.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)!!
        }

        val view: View = inflater.inflate(R.layout.fragment_login, container, false)

        usernameEditText = view.findViewById(R.id.username)
        passwordEditText = view.findViewById(R.id.password)
        keepLogged = view.findViewById(R.id.checkKeepLogged)
        mFirebaseAuth = FirebaseAuth.getInstance()
        loginButton = view.findViewById(R.id.btn_LogIn)

        val keepLogged = mSharedPreference?.getBoolean(KEEP_LOGGED_FIELD, false)
        if(keepLogged != null && keepLogged == true){

             val email = mSharedPreference?.getString(EMAIL_FIELD, "")
             val password = mSharedPreference?.getString(PASSWORD_FIELD, "")
             this.usernameEditText?.setText(email, TextView.BufferType.EDITABLE)
             this.passwordEditText?.setText(password, TextView.BufferType.EDITABLE)

            val intent = Intent (activity,HomeActivity::class.java)
            activity?.startActivity(intent);

            removeFragment();
        }

        loginButton?.setOnClickListener(View.OnClickListener() {
            loginButtonAction()

        })

        initRegisterTextView(view)
        return view
    }

    private fun initRegisterTextView(view: View)
    {
        register = view.findViewById(R.id.textView_register) as TextView
        if(register != null)
        {
            register!!.setOnClickListener(View.OnClickListener() {

                val registerFragmentName = RegisterFragmentName()
                activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, registerFragmentName).addToBackStack("REGISTER_FRAGMENT").commit()


            })
        }
        else
        {
            Toast.makeText(activity,"Register textView was empty",Toast.LENGTH_SHORT).show();
        }

    }

    private fun loginButtonAction() {

        val usernameString =  usernameEditText!!.text.toString()
        val passwordString = passwordEditText!!.text.toString()
        val keepLoggedChecked = keepLogged?.isChecked

        if(usernameString.isEmpty())
        {
            usernameEditText!!.error = "Please enter your username"
            usernameEditText!!.requestFocus()
        }
        else if(passwordString.isEmpty())
        {
            passwordEditText!!.error = "Please enter your password"
            passwordEditText!!.requestFocus()
        }
        else if(!(usernameString.isEmpty() && passwordString.isEmpty()))
        {
            activity?.let {
                mFirebaseAuth?.signInWithEmailAndPassword(usernameString, passwordString)
                    ?.addOnCompleteListener(it) { task ->
                        if (task.isSuccessful)
                        {
                            if(keepLoggedChecked == true){

                                val editor = mSharedPreference?.edit()
                                editor?.putString(EMAIL_FIELD, usernameString)
                                editor?.putString(PASSWORD_FIELD, passwordString)
                                editor?.putBoolean(KEEP_LOGGED_FIELD, keepLoggedChecked)
                                editor?.apply()
                            }


                            val intent = Intent (activity,HomeActivity::class.java)
                            activity?.startActivity(intent);
                            Toast.makeText(activity,"SignIn Successful",Toast.LENGTH_SHORT).show();

                            removeFragment()

                        } else
                        {
                            Toast.makeText(activity,"SignIn Unsuccessful, please try again",Toast.LENGTH_SHORT).show();
                        }

                    }
            }

        }
    }



    private fun removeFragment(){
        var fragment = fragmentManager?.findFragmentById(R.id.fragment_container)
        if(fragment is LoginFragment){
            var fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.remove(fragment)
            fragmentTransaction?.commit()
        }
    }

}
