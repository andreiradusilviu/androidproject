package com.example.accessmail.authentication

import android.R.attr.password
import android.content.Context
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.accessmail.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth


class RegisterFragmentCreateMailAddress : Fragment() {

    var buttonToRegisterCreatePassword: Button? = null
    var mailEditTex: EditText? = null
    var isValidMail = false
    var isUnusedMail = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_register_create_mail_address, container, false)

        mailEditTex = view.findViewById(R.id.mailAddress)
        isValidMail = false
        isUnusedMail = false

        buttonToRegisterCreatePassword = view.findViewById(R.id.btnToRegisterCreatePassword)
        goToRegisterFragmentCreatePassword(view)
        return view
    }

    private fun goToRegisterFragmentCreatePassword(view: View){
        buttonToRegisterCreatePassword!!.setOnClickListener(View.OnClickListener {

            var mailString: String = ""
            if(mailEditTex != null) {
                mailString = mailEditTex!!.text.toString()

                if(mailString == "")
                {
                    mailEditTex!!.error = "Insert your mail here"
                    mailEditTex!!.requestFocus()
                }
                else if(!isValidMailCheck(mailString))
                {
                    mailEditTex!!.error = "Please write an valid email adress"
                    mailEditTex!!.requestFocus()
                }
                else
                {
                    val bundle = arguments
                    bundle?.putString("email", mailString)

                    val registerFragmentCreatePassword = RegisterFragmentCreatePassword()
                    registerFragmentCreatePassword.arguments=bundle

                    if(activity != null)
                    {
                        val transaction = activity?.supportFragmentManager?.beginTransaction()
                        if (transaction != null) {
                            transaction.add(R.id.fragment_container, registerFragmentCreatePassword)
                            transaction.addToBackStack("REGISTER_FRAGMENT_CREATE_PASSWORD").commit()
                        }
                    }

                }
            }
        })
    }

    private fun isValidMailCheck(mail: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(mail).matches()



}
