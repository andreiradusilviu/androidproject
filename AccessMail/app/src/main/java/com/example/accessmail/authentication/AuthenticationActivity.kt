package com.example.accessmail.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.accessmail.R
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        val loginFragment:LoginFragment = LoginFragment()

        addFragment(loginFragment)
    }

    private fun addFragment(fragment:Fragment)
    {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,fragment).commit()
    }
}
