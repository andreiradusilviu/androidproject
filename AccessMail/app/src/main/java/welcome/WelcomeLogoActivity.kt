package welcome

import android.app.ActionBar
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.TextureView
import android.view.WindowManager
import android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.example.accessmail.R
import com.example.accessmail.authentication.AuthenticationActivity
import kotlinx.android.synthetic.main.activity_welcome_logo.*

class WelcomeLogoActivity : AppCompatActivity() {

    val SPLASH_SCREEN:Long = 4000
    var topAnimation: Animation?= null
    var bottomAnimation: Animation?= null
    var textView: TextView?= null
    var imageLogo: ImageView?= null
    var sloganLogo: TextView?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_logo)

        topAnimation = AnimationUtils.loadAnimation(this, R.anim.top_animation)
        bottomAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom_animation)
        imageLogo = findViewById(R.id.logoImage)
        sloganLogo = findViewById(R.id.textViewLogo)

        if(imageLogo!=null) {
            imageLogo!!.animation = topAnimation
        }

        if(sloganLogo!=null) {
            sloganLogo!!.animation = bottomAnimation
        }

        Handler().postDelayed(Runnable {
            val intent:Intent = Intent(this, AuthenticationActivity::class.java)
            startActivity(intent)
            finish()
        }, SPLASH_SCREEN)

    }
}


