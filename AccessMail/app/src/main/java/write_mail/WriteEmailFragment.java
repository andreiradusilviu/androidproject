package write_mail;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.accessmail.R;
import com.google.firebase.auth.FirebaseAuth;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;

import database.Email;
import database.EmailFirebaseDBService;
import home_activity.HomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteEmailFragment extends Fragment {

    private EditText _receiver;
    private EditText _subject;
    private EditText _context;
    private Button _sendBtn;

    public WriteEmailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_write_email, container, false);

        _receiver = view.findViewById(R.id.editText_to);
        _subject = view.findViewById(R.id.editText_subject);
        _context = view.findViewById(R.id.editText_message);
        _sendBtn = view.findViewById(R.id.btn_send);

        _sendBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                final Email email = new Email();

                LocalDateTime now = LocalDateTime.now();

                email.setDate(now.toString());

                email.setSender(FirebaseAuth.getInstance().getCurrentUser().getEmail());

                if(_subject.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG);
                    return;
                }
                email.setTitle(_subject.getText().toString());

                if(_context.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG);
                    return;
                }
                email.setContext(_context.getText().toString());

                if(_receiver.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG);
                    return;
                }
                email.setReceiver(_receiver.getText().toString());

                AddEmailInFireBase addEmailInFireBase = new AddEmailInFireBase();
                addEmailInFireBase.execute(email);

                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }

    private class AddEmailInFireBase extends AsyncTask<Email,Void,Void>
    {
        @Override
        protected Void doInBackground(Email... emails) {
            new EmailFirebaseDBService().addEmail(emails[0], new EmailFirebaseDBService.DataStatus() {
                @Override
                public void DataIsLoaded(ArrayList<Email> emails, ArrayList<String> keys) {

                }

                @Override
                public void DataIsInserted() {
                }

                @Override
                public void DataIsUpdated() {

                }

                @Override
                public void DataIsDeleted() {

                }


            });

            return null;
        }
    }
}
