package home_activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.AsyncTask;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.accessmail.R;
import com.example.accessmail.authentication.LoginFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import database.Email;
import database.EmailDB;
import database.EmailFirebaseDBService;
import home_activity.recycler_view.RecyclerAdapter;
import write_mail.WriteEmailFragment;

public class HomeActivity<var> extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCE_INFO";
    private final String EMAIL_FIELD = "EMAIL";
    private final String PASSWORD_FIELD = "PASSWORD";
    private final String KEEP_LOGGED_FIELD = "KEEP_LOGGED";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ArrayList<Integer> images;
    private DrawerLayout drawerLayout;


    public static EmailDB database;


    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initDB();

        initRecyclerView();

        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout_home);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_write:
                getSupportFragmentManager().beginTransaction().add(R.id.drawer_layout_home, new WriteEmailFragment()).addToBackStack("WRITE_EMAIL_TAG").commit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void initDB() {
        database = Room.databaseBuilder(getApplicationContext(), EmailDB.class, "EmailsDB4").allowMainThreadQueries().build();

        new EmailFirebaseDBService().readEmails(new EmailFirebaseDBService.DataStatus() {
            @Override
            public void DataIsLoaded(ArrayList<Email> emails, ArrayList<String> keys) throws ExecutionException, InterruptedException {

                boolean ok = false;

                database.dao().clear();

                for(Email email : emails)
                {
                    email.setImageId(R.drawable.background_01);

                    String currentClientName = FirebaseAuth.getInstance().getCurrentUser().getEmail();

                    if(email.getReceiver().equals(currentClientName))
                    {
                        database.dao().addEmail(email);
                        ok = true;
                    }
                }

                if(ok == true)
                {
                    adapter.notifyDataSetChanged();
                    notification();
                }
            }

            @Override
            public void DataIsInserted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDeleted() {

            }
        });


    }




    private class DisplayLocalDBMailByPosition extends AsyncTask<Integer,Void,Void>
    {
        Email email = null;
        @Override
        protected Void doInBackground(Integer... integers) {
            ArrayList<Email> emails = new ArrayList<>();
            emails = (ArrayList<Email>) database.dao().getEmails();

            if(emails.size() > 0)
                email = emails.get(integers[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DisplayMail(email);
        }

        private void DisplayMail(Email email)
        {
            if(email != null) {
                Bundle bundle = new Bundle();
                bundle.putString("sender", email.getSender());
                bundle.putString("title", email.getTitle());
                bundle.putString("date", email.getDate());
                bundle.putString("message", email.getContext());

                DisplayMailFragment displayMailFragment = new DisplayMailFragment();
                displayMailFragment.setArguments(bundle);

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.drawer_layout_home, displayMailFragment)
                        .addToBackStack("")
                        .commit();
            }
        }
    }

    private void initRecyclerView()
    {
        recyclerView = findViewById(R.id.recyclerView);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        images = new ArrayList<Integer>();
        images.add(R.drawable.plain);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);
        images.add(R.drawable.plain2);

        adapter = new RecyclerAdapter(images);

        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                new DisplayLocalDBMailByPosition().execute(position);

            }
        });
    }

    private void notification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("n", "n", NotificationManager.IMPORTANCE_DEFAULT);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "n")
                    .setContentText("Code Sphere")
                    .setSmallIcon(R.drawable.plain)
                    .setAutoCancel(true)
                    .setContentText("New Data is added");

            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
            managerCompat.notify(999, builder.build());
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_sign_out: {
                setDefaultValuesToSharedPreferences();
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                FirebaseAuth.getInstance().signOut();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.drawer_layout_home, new LoginFragment()).addToBackStack("SIGN_IN_FRAGMENT").commit();
            }
            break;

            case R.id.navigation_profile: {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.drawer_layout_home, new ClientProfileFragment()).addToBackStack("PROFILE_FRAGMENT").commit();
            }
            break;

            case R.id.navigation_sent_message: {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.drawer_layout_home, new SentMessagesFragment()).addToBackStack("SENT_MESSAGES_FRAGMENT").commit();
            }
            break;

            case R.id.navigation_unread_message: {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.drawer_layout_home, new UnreadMessagesFragment()).addToBackStack("UNREAD_MESSAGES_FRAGMENT").commit();
            }
            break;
        }

        return true;
    }

    private void setDefaultValuesToSharedPreferences() {

        SharedPreferences mSharedPreference = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putString(EMAIL_FIELD, "");
        editor.putString(PASSWORD_FIELD, "");
        editor.putBoolean(KEEP_LOGGED_FIELD, false);
        editor.apply();

    }
}
