package home_activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.textclassifier.TextLinks;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.accessmail.R;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientProfileFragment extends Fragment {

    private TextView _birthdate;
    private TextView _email;
    private TextView _name;
    private TextView _gender;

    public ClientProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_profile, container, false);

        getClientData();

        _name = view.findViewById(R.id.tv_title_ClientProfile);
        _email = view.findViewById(R.id.client_profile_email);
        _birthdate = view.findViewById(R.id.client_profile_DOB);
        _gender = view.findViewById(R.id.profile_gender_tv);

        return view;
    }

    public void getClientData()
    {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String url = "https://accessmail-9934e.firebaseio.com/User.json";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    handleClientDataResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);
    }

    public void handleClientDataResponse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);

        Iterator<String> keys = jsonObject.keys();

        ArrayList<String> clientKeys = new ArrayList<String>();

        while (keys.hasNext())
        {
            clientKeys.add(keys.next());
        }

        JSONArray jsonArray = jsonObject.toJSONArray(new JSONArray(clientKeys));

        for(int index=0;index<jsonArray.length();index++)
        {
            JSONObject jsonObject1 = jsonArray.getJSONObject(index);

            String uid = jsonObject1.getString("uid");

            if(uid.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
            {
                String birhdate = jsonObject1.getString("date");
                String email = jsonObject1.getString("email");
                String firstName = jsonObject1.getString("firstName");
                String lastName = jsonObject1.getString("lastName");
                String gender = jsonObject1.getString("gender");

                _name.setText(firstName + " " + lastName);
                _email.setText(email);
                _gender.setText(gender);
                _birthdate.setText(birhdate);
            }
        }
    }
}
