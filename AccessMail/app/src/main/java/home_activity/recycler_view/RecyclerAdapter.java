package home_activity.recycler_view;

import android.graphics.Color;
import android.os.Build;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.accessmail.R;

import java.util.ArrayList;

import home_activity.HomeActivity;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.EmailViewHolder> {

    private ArrayList<Integer> images;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public RecyclerAdapter(ArrayList<Integer> images)
    {
        this.images = images;
    }

    @NonNull
    @Override
    public EmailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.email_view_layout,parent,false);

        EmailViewHolder emailViewHolder = new EmailViewHolder(view, mListener);

        return emailViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onBindViewHolder(@NonNull EmailViewHolder holder, int position)
    {
        holder.profile_imageView.setImageResource(HomeActivity.database.dao().getEmails().get(position).getImageId());

        SpannableString string = new SpannableString(" ");
        //string.setSpan(new BulletSpan(0, Color.BLUE, 10), 0, 1,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.bullet_textView.setText(string);
        holder.sender_textView.setText(HomeActivity.database.dao().getEmails().get(position).getSender());
        holder.title_textView.setText(HomeActivity.database.dao().getEmails().get(position).getTitle());
        holder.description_textView.setText(HomeActivity.database.dao().getEmails().get(position).getContext());
        holder.emailDate_textView.setText(HomeActivity.database.dao().getEmails().get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return HomeActivity.database.dao().getEmails().size();
    }

    public static class EmailViewHolder extends RecyclerView.ViewHolder
    {

        ImageView profile_imageView;
        TextView sender_textView;
        TextView title_textView;
        TextView description_textView;
        TextView bullet_textView;
        TextView emailDate_textView;

        public EmailViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            emailDate_textView = itemView.findViewById(R.id.email_date);
            bullet_textView = itemView.findViewById(R.id.bullet_textView);
            profile_imageView = itemView.findViewById(R.id.profile_image);
            sender_textView = itemView.findViewById(R.id.sender_textView);
            title_textView = itemView.findViewById(R.id.title_textView);
            description_textView = itemView.findViewById(R.id.description_textView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
