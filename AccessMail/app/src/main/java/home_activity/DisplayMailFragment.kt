package home_activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.accessmail.R

/**
 * A simple [Fragment] subclass.
 */
class DisplayMailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_display_mail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val titleTextView: TextView = view.findViewById(R.id.tv_title)
        val dateTextView: TextView = view.findViewById(R.id.tv_date)
        val senderTextView: TextView = view.findViewById(R.id.tv_from)
        val messageTextView: TextView = view.findViewById(R.id.tv_message)

        val bundle: Bundle? = arguments;
        if (bundle != null)
        {
            val title = bundle.getString("title")
            val date = bundle.getString("date")
            val sender = bundle.getString("sender")
            val message = bundle.getString("message")

            if(!title.isNullOrEmpty() && !date.isNullOrEmpty() && !sender.isNullOrEmpty() && !message.isNullOrEmpty())
            {
                titleTextView.text = title
                dateTextView.text = date
                senderTextView.text = sender
                messageTextView.text = message
            }
        }
    }

}
